package com.example.tiendaj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TiendajApplication {

    public static void main(String[] args) {
        SpringApplication.run(TiendajApplication.class, args);
    }

}
