package com.example.tiendaj.repository;

import com.example.tiendaj.entity.Cliente;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClienteRespository extends MongoRepository<Cliente, String> {

}
