package com.example.tiendaj.repository;

import com.example.tiendaj.entity.Producto;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ProductRepository extends MongoRepository<Producto, String> {

}
