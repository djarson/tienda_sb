package com.example.tiendaj.repository;

import com.example.tiendaj.entity.Carrito;
import com.example.tiendaj.entity.Cliente;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface CarritoRepository extends MongoRepository<Carrito, String> {
    List<Carrito> findByCliente(String cliente);

}
