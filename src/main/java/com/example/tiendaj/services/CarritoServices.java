package com.example.tiendaj.services;

import com.example.tiendaj.entity.Carrito;
import com.example.tiendaj.entity.Cliente;
import com.example.tiendaj.repository.CarritoRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarritoServices {

    private final CarritoRepository carritoRepository;

    @Autowired
    public CarritoServices(CarritoRepository carritoRepository) {
        this.carritoRepository = carritoRepository;
    }

    public List<Carrito> getAllCarrito(){
        return carritoRepository.findAll();
    }
    public List<Carrito> getCarritosByCliente(String clienteId) {

        return carritoRepository.findByCliente(clienteId);
    }
}

