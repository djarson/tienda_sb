package com.example.tiendaj.services;

import com.example.tiendaj.entity.Producto;
import com.example.tiendaj.repository.ProductRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoServices {

    @Autowired
    private ProductRepository  productRepository;


    public List<Producto> getAllProduct(){
        return productRepository.findAll();
    }

    public Optional<Producto> getAllProductId(String id){
        return productRepository.findById(id);
    }

}
