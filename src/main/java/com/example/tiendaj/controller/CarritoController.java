package com.example.tiendaj.controller;

import com.example.tiendaj.entity.Carrito;
import com.example.tiendaj.entity.Cliente;
import com.example.tiendaj.services.CarritoServices;
import com.example.tiendaj.services.ClienteServices;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/car")
public class CarritoController {

    private final CarritoServices carritoServices;
    private final ClienteServices clienteServices;

    public CarritoController(CarritoServices carritoServices, ClienteServices clienteServices) {
        this.carritoServices = carritoServices;
        this.clienteServices = clienteServices;
    }

    @GetMapping("/cliente/{clienteId}")
    public ResponseEntity<List<Carrito>> getCarritosByCliente(@PathVariable String clienteId) {
        List<Carrito> carritos = carritoServices.getCarritosByCliente(clienteId);
        return ResponseEntity.ok(carritos);
    }
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Carrito> getCarritoCliente(){
        return carritoServices.getAllCarrito();
    }



}

