package com.example.tiendaj.controller;

import com.example.tiendaj.entity.Cliente;
import com.example.tiendaj.entity.Producto;
import com.example.tiendaj.services.ClienteServices;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {

    @Autowired
    private ClienteServices clienteServices;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Cliente> obtenerProductos(){
        return clienteServices.getAllCliente() ;

    }
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<Cliente> getAllClientId(@PathVariable String id){
        return clienteServices.getAllCliente();
    }
}
