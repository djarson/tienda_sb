package com.example.tiendaj.controller;

import com.example.tiendaj.entity.Producto;
import com.example.tiendaj.services.ProductoServices;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/list")
public class ProductoController {

    @Autowired
    private ProductoServices productoServices;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Producto> obtenerProductos(){
        return productoServices.getAllProduct() ;

    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Optional<Producto> obtenerProductosId(@PathVariable String id){
        return productoServices.getAllProductId(id);

    }
}
