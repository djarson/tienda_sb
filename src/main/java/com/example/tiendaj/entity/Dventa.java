package com.example.tiendaj.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(value = "dventas")
@Getter
@Setter
@NoArgsConstructor
public class Dventa {
    @Id
    private String id;
    @DBRef
    private Cliente cliente;
    @DBRef
    private Producto producto;
    @DBRef
    private Venta venta;

    private Integer subtotal;

    private String variedad;

    private Integer cantidad;

    private Date createAt;
}
