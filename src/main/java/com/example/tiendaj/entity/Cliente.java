package com.example.tiendaj.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Document(collection="clientes")


public class Cliente {

    @Id
    private String id;

    private String nombres;
    private String apellidos;
    private String pais;
    private String email;
    private String password;
    private String telefono;
    private String genero;
    private String f_nacimiento;
    private String dni;
    private Date createAt;
}
