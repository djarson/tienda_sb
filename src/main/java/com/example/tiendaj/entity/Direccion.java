package com.example.tiendaj.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(value = "direccions")
@Getter
@Setter
@NoArgsConstructor
public class Direccion {
    @Id
    private String id;
    @DBRef
    private Cliente cliente;
    private String destinatario;
    private String dni;
    private String zip;
    private String direccion;
    private String pais;
    private String region;
    private String provincia;
    private String distrito;
    private String telefono;
    private Boolean principal;
    private Date createAt;


}
