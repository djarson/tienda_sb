package com.example.tiendaj.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(value = "ventas")
@Getter
@Setter
@NoArgsConstructor
public class Venta {
    @Id
    private String id;
    @DBRef
    private Cliente cliente;

    private String nventas;
    private String envio_titulo;
    private Double envio_precio;
    private String transaccion;
    private String cupon;
    private String estado;
    private String direccion;
    private Double subtotal;
    private String nota;
    private String createAt;



}
