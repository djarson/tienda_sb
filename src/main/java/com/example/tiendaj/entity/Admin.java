package com.example.tiendaj.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="admins")
@Getter
@Setter
@NoArgsConstructor
public class Admin {
    @Id
    private String id;
    private String nombres;
    private String apellidos;
    private String email;
    private String password;
    private String telefono;
    private String rol;
    private String dni;




}
