package com.example.tiendaj.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(value="cupons")
@Getter
@Setter
@NoArgsConstructor
public class Cupon {
    @Id
    private String id;
    private String codigo;
    private String tipo;
    private String valor;

    private Integer limite;
    private Date createAt;
}
