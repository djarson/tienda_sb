package com.example.tiendaj.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Document(collection="carritos")
@Getter
@Setter
@NoArgsConstructor
public class Carrito {
    @Id
    private String id;

    @DBRef
    private List<Cliente> cliente;
    @DBRef
    private List<Producto> producto;

    private String cantidad;
    private String variedad;
    private Date createdAt;

}
