package com.example.tiendaj.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection="configs")
@Getter
@Setter
@NoArgsConstructor
public class Config {
    @Id
    private String id;
    private List<Object> categorias;
    private String titulo;
    private String logo;
    private String serie;
    private String correlativo;





}
