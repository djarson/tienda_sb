package com.example.tiendaj.entity;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Document(value = "productos")

public class Producto {

    @Id
    private String id;

    private String titulo;
    private String slug;
//    private String galeria;
    private List<Object> galeria;
    private String portada;
    private Double precio;
    private String descripcion;
    private String contenido;
    private String stock;
    private int nventas;
    private int npuntos;
//    private String variedad;
    private List<Object> variedades;
    private String categoria;
    private String titulo_variedad;
    private String estado;
    private Date createAt;
}
