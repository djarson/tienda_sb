package com.example.tiendaj.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(value = "inventarios")
@Getter
@Setter
@NoArgsConstructor
public class Inventario {
    @Id
    private String id;
    @DBRef
    private Producto producto;
    private String cantidad;
    @DBRef
    private Admin admin;
    private String proveedor;

    private Date createAt;


}
